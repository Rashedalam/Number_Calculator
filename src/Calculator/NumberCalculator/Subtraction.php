<?php

namespace Pondit\Calculator\NumberCalculator;

class Subtraction
{
    public $number1;
    public $number2;

    public function sub2numbers($number1, $number2)
    {
        return $this->number1 - $this->number2;
    }
}